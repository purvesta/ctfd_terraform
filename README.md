# CTFd Terraform for AWS
Add the following blocks with your credentials inserted to the vars.tf file:

```
###############
# Credentials #
###############

variable "access_key" {
  description = "AWS access key"
  default     = "redacted"
}

variable "secret_key" {
  description = "AWS secret access key"
  default     = "redacted"
}

variable "account_id" {
  description = "Cloudflare account_id"
  default     = "redacted"
}

variable "api_token" {
  description = "Cloudflare api_token"
  default     = "redacted"
}

variable "zone_id" {
  description = "Cloudflare zone_id"
  default     = "redacted"
}
```

Then from the top level directory run:

`terraform init`  
`terraform apply`
