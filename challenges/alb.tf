resource "aws_lb" "challenge-alb" {
  name               = var.service
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb-sg.id]
  subnets            = module.vpc.public_subnets

  enable_deletion_protection = false

#   access_logs {
#     bucket  = aws_s3_bucket.challenge-access.bucket
#     prefix  = "${each.value}-alb"
#     enabled = true
#   }

  tags = {
    Name = "${var.service}-alb"
  }
}

resource "aws_lb_target_group" "challenge-tg" {
  count       = length(var.challenges)
  name        = "${var.challenges[count.index]}-tg"
  port        = var.container_port
  protocol    = "HTTP"
  vpc_id      = module.vpc.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = 3
    interval            = 30
    protocol            = "HTTP"
    matcher             = 200
    timeout             = 3
    path                = var.health_check
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.challenge-alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "redirect"

    redirect {
      port        = 443
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "https" {
 load_balancer_arn = aws_lb.challenge-alb.arn
 port              = 443
 protocol          = "HTTPS"

 ssl_policy        = "ELBSecurityPolicy-2016-08"
 certificate_arn   = var.alb_cert_arn

 default_action {
    ###################
    # CTFd scoreboard #
    ###################
    # type             = "forward"
    # target_group_arn = aws_lb_target_group.challenge-tg[count.index].arn
    type               = "fixed-response"
    fixed_response {
      content_type = "text/html"
      message_body = "<script>fetch('https://live.neverlanctf.com/api/v1/witticism').then(res => res.json()).then(res => {document.write(res.data.description)});</script>"
      status_code  = "200"
    }
  }
}

resource "aws_lb_listener_rule" "path_monitoring" {
  count              = length(var.challenges)
  listener_arn       = aws_lb_listener.https.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.challenge-tg[count.index].arn
  }

  # When they request the challenge name, forward them to the right set of containers
  # condition {
  #   path_pattern {
  #     values = ["/${var.challenges[count.index]}"]
  #   }
  # }

  # Must send request to official domain and not directly to LB
  condition {
    host_header {
      values = ["${var.challenges[count.index]}.purvesta.io"]
    }
  }
}
