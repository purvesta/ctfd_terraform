output "alb_dns_name" {
  description = "ALB DNS name"
  value       = aws_lb.challenge-alb.dns_name
}