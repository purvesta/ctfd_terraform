variable "service" {
  description = "The name of this service"
  default     = "challenges"
}

variable "challenges" {
  description = "A list of challenge names (must be the same as the challenge file names)"
  default     = ["cookie-monster", "sql-fun1", "js-pass"]
}

variable "container_port" {
  description = "The port for all containers to listen on"
  default     = 80
}

variable "vpc_cidr" {
  description = "CIDR for VPC"
  default     = "10.0.0.0/16"
}

variable "public_subnets" {
  description = "A list of public subnets"
  default     = ["10.0.101.0/24", "10.0.102.0/24"]
}

variable "private_subnets" {
  description = "A list of private subnets"
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "azs" {
  description = "A list of azs"
  default     = ["us-west-2a", "us-west-2b"]
}

variable "alb_cert_arn" {
  description = "The arn of the alb cert."
  default     = "arn:aws:acm:us-west-1:848838107372:certificate/c7e3adce-92d0-42f3-a631-bf34b44204a6"
}

variable "health_check" {
  description = "The path of the container health check"
  default     = "/"
}

variable "zone_id" {
  description = "Cloudflare zone id"
  default     = "123456789"
}