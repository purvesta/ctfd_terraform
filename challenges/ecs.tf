resource "aws_ecs_cluster" "challenges" {
  name = "challenges"
}

resource "aws_ecs_task_definition" "challenge-task" {
  count                    = length(var.challenges)
  family                   = var.challenges[count.index]
  task_role_arn            = aws_iam_role.ecs-task-role.arn
  execution_role_arn       = aws_iam_role.ecs-task-role.arn
  network_mode             = "awsvpc"
  cpu                      = "1024"
  memory                   = "2048"
  requires_compatibilities = ["FARGATE"]
  container_definitions    = file("${path.module}/task-definitions/${var.challenges[count.index]}.json")
}


resource "aws_ecs_service" "challenge" {
  count                             = length(var.challenges)
  name                              = var.challenges[count.index]
  cluster                           = aws_ecs_cluster.challenges.id
  task_definition                   = aws_ecs_task_definition.challenge-task[count.index].arn
  desired_count                     = 1
  launch_type                       = "FARGATE"
  scheduling_strategy               = "REPLICA"
  platform_version                  = "1.4.0"
  health_check_grace_period_seconds = 10
  depends_on                        = [aws_iam_role.ecs-task-role, aws_security_group.web-challenge-sg, aws_lb_target_group.challenge-tg, aws_lb_listener.http, aws_lb_listener.https]


  load_balancer {
    target_group_arn = aws_lb_target_group.challenge-tg[count.index].arn
    container_name   = var.challenges[count.index]
    container_port   = var.container_port
  }
  network_configuration {
    assign_public_ip = "false"
    security_groups  = [aws_security_group.web-challenge-sg.id]
    subnets          = module.vpc.private_subnets
  }

  lifecycle {
    ignore_changes = [desired_count]
  }
}
