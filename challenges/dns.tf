# resource "aws_route53_zone" "challenges" {
#   name = "purvesta.io"
# }

# resource "aws_route53_record" "challenges" {
#   count   = length(var.challenges)
#   zone_id = aws_route53_zone.challenges.zone_id
#   name    = var.challenges[count.index]
#   type    = "CNAME"
#   ttl     = "5"
#   depends_on = [aws_route53_zone.challenges, aws_lb.challenge-alb]

#   records        = [aws_lb.challenge-alb.dns_name]
# }

# resource "digitalocean_record" "www" {
#   domain = digitalocean_domain.default.name
#   type   = "A"
#   name   = "www"
#   value  = "192.168.0.11"
# }

resource "cloudflare_record" "challenges" {
  count   = length(var.challenges)
  zone_id = var.zone_id
  name    = var.challenges[count.index]
  value   = aws_lb.challenge-alb.dns_name
  proxied = false
  type    = "CNAME"
  ttl     = 3600
}