output "alb_dns_name" {
  description = "ALB DNS name"
  value       = module.challenges.alb_dns_name
}