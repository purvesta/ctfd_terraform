############
# Defaults #
############

variable "ctfd-ami" {
  description = "AMI to use for ctfd instances"
  default     = "ami-0e4035ae3f70c400f"
}

variable "region" {
  description = "AWS region to use"
  default     = "us-west-1"
}