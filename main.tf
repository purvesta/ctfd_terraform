terraform {
  required_version = ">= 0.13"
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "2.17.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "3.9.0"
    }
  }
}

provider "cloudflare" {
  account_id = var.account_id
  api_token  = var.api_token
}

provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

# module "ctfd" {
#   source         = "./ctfd"
#   region         = var.region
#   az1            = var.az1
#   az2            = var.az2

#   # Comment out to default to us-west-2
#   ctfd-ami       = var.ctfd-ami
#   awx-ami        = var.ctfd-ami
#   elb-account-id = "027434742980"

#   # Change password before running prod environment
#   dbuser         = "ctfd"
#   dbpass         = "hackallthethings"
# }

module "challenges" {
  source     = "./challenges"
  # REPLACE ALL _ with - for the sake of challenge names. Terraform/AWS won't allow for _ for some reason...
  challenges = ["cookie-monster", "sql-fun1", "js-pass"]
  zone_id    = var.zone_id
  azs        = ["us-west-1b", "us-west-1c"]
}
